<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tankdb');

/** MySQL database username */
define('DB_USER', 'tankdev');

/** MySQL database password */
define('DB_PASSWORD', 'tankdev');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WPMDB_LICENCE', '1d5e9e7b-e36a-471b-94b7-7b70418ab201' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xs4|1VSk5%|}8@Z/G)5u]Vu^dMpM5E]3m^r_`e;==(a,q-ob<o%G$|SF(6 BXZ#k');
define('SECURE_AUTH_KEY',  'EfjehoG`T@I~36VptI*K.2xK{D!Q~MmACyE.2{/Y^5wGS1e>PBPNGxAmCle7>yNW');
define('LOGGED_IN_KEY',    ']SpA^xeP,VYT5lx[MrK0w??IiUF71B#xzyTZ^Z >u{JM>4Sn i!>fKKkiXyI8&sw');
define('NONCE_KEY',        '%^X9$q@qJE~0a1R<bmXq/|csJ1)mk!}+tO{x[/&aEX3_*{(lo0CX8g]wtZI8`(0N');
define('AUTH_SALT',        '#dR:TBpXB5bL=d-S|)Vg+:XZdG#6HJ}T!>dE4>-TF51p&Z+(D}~H%RTkA?Ps)YC>');
define('SECURE_AUTH_SALT', 'TLK8.RqZpe:/u~&Af0vuGSJOXIQ&BBv@g-Fy&Y ]M,6UzG+D^Sc*:Z7#({?_40tV');
define('LOGGED_IN_SALT',   '<{,d,dG(**(G57KCQnZa/sz KZwNvJP)]L8]f?v4FY:8BAmA&k5@s2tR.j:tx1ic');
define('NONCE_SALT',       'rg%|vxH%Am%eGy&|<4/p[F0<T1DarzH.pCg{p{qTX;PpP/BVE#%N11GLd;;&YgFK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

