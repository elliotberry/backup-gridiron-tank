const webpack = require('webpack');

const path = require('path')
const uglify = require('uglifyjs-webpack-plugin')
const extractText = require('extract-text-webpack-plugin');

const themePath = './webroot/wp-content/themes/gridiron/assets/js/';


module.exports = {

    entry: ['@babel/polyfill', themePath + 'main.js' ],
    devtool: 'source-map',

    output: {
        filename: 'main.min.js',
        path: path.resolve(__dirname, themePath)
    },

    module: {
        rules: [
          {
            enforce: "pre",
            test: /\.scss?$/,
            exclude: /(node_modules)/,
            loader: "import-glob-loader"
          },
          {
              test: /\.(ttf|eot|woff|woff2)$/,
              use: {
                loader: "url-loader",
                options: {
                  name: "fonts/[name].[ext]",
                },
              },
            },
            {
                test: /\.scss$/,
                exclude: /(node_modules)/,
                use: extractText.extract(
                    {
                        fallback: 'style-loader',

                        use: [
                            {
                                loader: 'css-loader',
                                options: { sourceMap: true, minimize: true }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: true,
                                    plugins: () => [
                                        require('autoprefixer')({
                                            'browsers': ['> 1%', 'last 2 versions']
                                        })
                                    ]
                                },
                            },
                            {
                                loader: 'sass-loader',
                                options: { sourceMap: true }
                            },
                        ],

                    })
            },
            {
              test: /\.(svg|png|gif|jpg)$/,
              loader: "file-loader",
              options: {
                name: "[name].[ext]",
                outputPath: "../",
                useRelativePath: true,
                emitFile: false
              }
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ["@babel/preset-env", { 
                                "modules": false, 
                                "targets": {
                                    "ie": "10"
                                } 
                            }]
                        ],
                        plugins: ["syntax-dynamic-import"]
                    }
                }

            }
        ]
    },

    plugins: [
        new extractText(
            { filename: '../css/style.min.css' }
        ),

        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),

        // new uglify({
        //    sourceMap: true,
        //    uglifyOptions: { mangle: true },
        // })
    ]
}
