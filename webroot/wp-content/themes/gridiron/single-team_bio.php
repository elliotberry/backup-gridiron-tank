<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gridiron
 */

get_header();
$current_id = get_the_id();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post();
      get_template_part( 'template-parts/breadcrumbs');
			get_template_part( 'template-parts/content', get_post_type() );

			if( get_post_type() != 'product' ):
				//the_post_navigation();
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					// comments_template();
				endif;
			endif;

		endwhile; // End of the loop.
		?>

		
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

if( get_post_type() != 'product' ):
	//get_sidebar();
endif;

get_footer();

?>

