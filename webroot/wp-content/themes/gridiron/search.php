<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package gridiron
 */

get_header();

function gridirion_render_no_results(){
	$the_search_query = get_search_query();
	global $wp_query;
	$search_results_count = $wp_query->found_posts;
	echo '<p>There were <strong>' . $search_results_count . '</strong> results for <strong>' . $the_search_query. '.</strong></p>';
	echo '<p>Didn&apos;t find what you were looking for? <a href="#">Search again</a> or <a href="#">Contact us.</a></p>';	
}
?>

	<section id="primary" class="search-page">
		<main id="main" class="site-main">
			<div class="container">
					<?php if ( have_posts() ) : ?>
						<header class="page-header">
							<h1 class="page-title">
								Search Results
							</h1>
							<div class="search-info">
								<?php gridirion_render_no_results(); ?>
							</div>
				
						</header><!-- .page-header -->
						<?php
						/* Start the Loop */
						while ( have_posts() ) :
							the_post();
							/**
							 * Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called content-search.php and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'search' );
						endwhile;
						
					else :
						gridirion_render_no_results(); 
					endif;
					?>
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
