<div class="posts__filter">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <label for="category" class="label">Filter by:</label>
                <div class="select-field-box">
                <div class="select-arrow-icon"><svg xmlns="http://www.w3.org/2000/svg" width="17.74" height="9.53" viewBox="0 0 17.74 9.53">
  <title>Asset 1</title>
  <polyline points="17.41 0.33 8.87 8.87 0.33 0.33" style="fill: none;stroke: #2d2a26;stroke-miterlimit: 10;stroke-width: 0.937092284020409px"/>
</svg>
</div>
                <select name="category" class="option-cat">
                    <option class="company-cat-all" value="all">

                        All

                    </option>
                    <?php $categories = get_categories();
foreach ($categories as $cat):
  
if ($cat->term_id !== 1):
?>
                    <option value="<?php echo createSlug($cat->name); ?>">

                        <?php echo $cat->name; ?>

                    </option>

                    <?php endif; endforeach;?>
                </select>
                </div>
            </div>

        </div>
    </div>
</div>