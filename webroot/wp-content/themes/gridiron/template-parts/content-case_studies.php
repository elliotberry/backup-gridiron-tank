
<article id="post-<?php the_ID();?>" <?php post_class();?>>
<div class="entry-content">
<div class="case-study--details company">
  <?php
  // global $post;
  // $company = get_field('company_referenced');
  // $post = $company[0];
  // setup_postdata($post);
  // wp_reset_postdata(); 

?>
<div class="container">
  <div class="row">

    <div class="col-md">
    <div class="case-study--title heading page-title"><?php the_title(); ?></div>

    <div class="block--content-block__subtitle case-study--subtitle_info">
    <?php field_div("case-study--hq", "hq", "<strong>HQ: </strong><span>{% field %}</span>"); ?>
    <?php field_div("case-study--date-of-investment", "date_of_investment", "<strong>Date of Investment: </strong><span>{% field %}</span>"); ?>
        <div class="case-study--industry">
          <?php 
          $industry_terms = get_the_terms($post->ID, 'industry');
          $industry_string = '';
          if ($industry_terms) {
            foreach ($industry_terms as $term) {
              $industry_string .= $term->name.' ';
            }
          ?>
          <strong>Industry: </strong><span><?php echo $industry_string; ?></span>
          <?php } ?>
        </div>
    </div>
    </div>
    <div class="case-study--image col-md">
        <?php gridiron_post_thumbnail(); ?>
    </div>
  </div>
</div>


</div>
<div class="container case-study__description">
  <?php the_field('description'); ?>
</div>
</div>
</article><!-- #post-<?php the_ID();?> -->
