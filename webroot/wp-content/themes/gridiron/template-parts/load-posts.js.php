var $ = jQuery;

var postsAJAX = {
    batchSize: <?php $batch = get_field('number_of_news_articles_to_load', 'option') ? get_field('number_of_news_articles_to_load', 'option') : 10; echo $batch; ?>,
    currentCat: "all",
    currentNumber: function() {
        return $('.posts__list-container .post-list-item').length;
    },
    cats: { <?php getPostCatInfo(); ?> }
};

function buttonStates() {
    if (postsAJAX.cats[postsAJAX.currentCat].count <= postsAJAX.currentNumber()) {
        $('.load-posts').removeClass("is--active");
    }
    else {
        $('.load-posts').addClass("is--active");
    }
    
}
function changePostsAnimation() {
    $('.posts__list-container').addClass('is--changing');
    setTimeout(function(){  $('.posts__list-container').removeClass('is--changing'); }, 1000);
}

$(document).ready(function () {

    buttonStates();
    $(".posts__filter select").on("change", function (e) {
        e.preventDefault();
        loadMorePosts(true, $(this).val());
    });


    $(".load-posts").on("click", function (e) {
        e.preventDefault();
        loadMorePosts(false, postsAJAX.currentCat);
    });


    function loadMorePosts(change, cat) {
        $('.load-posts').removeClass("is--active");
        if (change === true) {
            postsAJAX.currentCat = cat;
            $('.posts__list-container .post-list-item').remove();
            thisOffset = 0;
        }
        else {
            thisOffset = postsAJAX.currentNumber;
        }
        var sendData = {
          action: "loadposts",
          page: gridiron.current_page,
          offset: thisOffset
        };
        if (cat !== "all") {
        var sendCat = postsAJAX.cats[cat].id;
            sendData.cat = sendCat;
        } 

        $.ajax({
            url: gridiron.ajaxurl,
            type: "post",
            data: sendData,
            beforeSend: function () {
                $('.posts__spinner').addClass("is--active");
                changePostsAnimation();
            },
            success: function (data) {
             $(".posts__list-container").append(data);
            
                $('.posts__spinner').removeClass("is--active");

                gridiron.current_page++;
                buttonStates();
            }
        });
    }

});

