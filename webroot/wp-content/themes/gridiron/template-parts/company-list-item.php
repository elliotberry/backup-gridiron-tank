<?php
$industry_terms = get_the_terms($post->ID, 'industry');
$industry_string = '';
foreach ($industry_terms as $term) {
  $industry_string .= $term->slug.' ';
}
$logo = get_field('logo');
?>

<div class="company-list-item post-list-item col-sm-6 is--active" data-status="all <?php the_field('current_or_past');?>" data-industries="all <?php echo $industry_string; ?>">
  <a class="company-list-item__wrap" href="<?php the_permalink();?>">
    <div class="company-list-item__image">
      <img src="<?php echo $logo['url'] ?>" alt="">
    </div>
    <div class="company-list-item__title">
      <h2>
        <?php the_title();?>
      </h2>
    </div>
    <?php if (get_field('location')): ?>
    <div class="company-list-item__hq">
      Headquarters:
      <?php the_field('location');?>
    </div>
    <?php endif; ?>
    <?php if (get_field('partnered_date')): ?>
    <div class="company-list-item__date">
      Partnered in:
      <?php the_field('partnered_date');
      ?>
    </div>
<?php endif; ?>
  </a>
</div>