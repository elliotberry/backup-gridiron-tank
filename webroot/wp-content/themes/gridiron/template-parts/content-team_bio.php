
<article class="post-team-bio" id="post-<?php the_ID();?>" <?php post_class();?>>
<div class="container">
	<header class="entry-header">
		<?php /*the_title( '<h1 class="entry-title">', '</h1>' ); */?>
	</header><!-- .entry-header -->



	<div class="entry-content">

    <div class="person--top row">
    <div class="col col-lg-6">
    <?php field_div("person--name heading block--content-block__title", "name"); ?>
    <?php field_div("person--title", "title"); ?> 
    <?php field_div("person--phone", "phone"); ?> 
    <?php field_div("person--email", "email"); ?> 
    <div class="person--social-icons">
    <?php

// check if the repeater field has rows of data
if( have_rows('social') ):

 	// loop through the rows of data
    while ( have_rows('social') ) : the_row();
    ?>
    <a target="_blank" href="<?php the_sub_field('url'); ?>" alt="<?php the_sub_field('link_type'); ?>" class="person--social-icon <?php the_sub_field('link_type'); ?>"></a>
    <?php endwhile; endif;
    if( get_field('email') ): ?>
    
    <a target="_blank" href="mailto:<?php the_field('email'); ?>" alt="Email <?php the_field('name'); ?>" class="person--social-icon email"></a>

    <?php
endif;
    
    ?>
    </div>
    </div>
    <div class="col col-lg-6 team-bio-pic">
    <?php gridiron_post_thumbnail(); ?>
</div></div>
    <?php field_div("person--description", "description"); ?>
</div></div>
	<!-- .entry-content -->

	<?php gridiron_render_blocks()?>
        </div>
</article>
