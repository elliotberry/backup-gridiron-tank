<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gridiron
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('search-list-item'); ?>>
  <a class="search-list-item__title" href="<?php the_permalink(); ?>">
    <h2><?php the_title();?></h2>
  </a>
	<div class="search-list-item__link">
    <a href="<?php the_permalink(); ?>"><?php the_permalink();?></a>
  </div>
  <div class="search-list-item__excerpt">
    <?php 
      if ( !empty( get_the_excerpt() ) ):
          the_excerpt();
      elseif ( !empty( get_field('description')) ):
          $desc = get_field('description');
          $descStrip = wp_strip_all_tags($desc);
          echo wp_trim_words($descStrip, 40, '[...]');
      elseif (have_rows('content_blocks')):
        $loop = 0;
        while (have_rows('content_blocks')): the_row();
           $desc = get_sub_field('body');
           $descStrip = wp_strip_all_tags($desc);
           echo wp_trim_words($descStrip, 40, '[...]');
           $loop++;
           if ($loop > 1):
            break;
           endif;
        endwhile;
      else:
          echo wp_trim_words( get_the_content(), 40, '[...]' ); 
      endif;
    ?>
  </div>
</article><!-- #post-<?php the_ID(); ?> -->
