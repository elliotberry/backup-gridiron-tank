            <div class="post-list-item row company-cat-all is--active cat-<?php the_category_ID();?>">
            <div class="container">
  <div class="post-list-item__date">
    <?php echo get_the_time('F Y'); ?>
  </div>
  <a class="post-list-item__title" href="<?php the_permalink(); ?>">
    <h2><?php the_title();?></h2>
  </a>
  <div class="post-list-item__cats">
    <?php gridiron_render_post_categories(get_the_ID(), true); ?>
  </div>
  <div class="post-list-item__excerpt">
    <?php the_excerpt(); ?>
  </div></div>
 
</div>