
<article id="post-<?php the_ID();?>" <?php post_class();?>>
    <div class="entry-content">
        <div class="company--details company">
            <?php get_template_part( 'template-parts/company-header'); ?>
        </div>
    </div><!-- .entry-content -->
    <?php gridiron_render_blocks()?>
</article><!-- #post-<?php the_ID();?> -->
