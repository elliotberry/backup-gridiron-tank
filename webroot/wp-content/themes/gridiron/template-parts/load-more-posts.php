<div class="posts__load-more-container">
  <div class="container">
  <div class="row justify-content-center">
    <a class="btn load-posts is--active" style="margin-bottom: 50px;">Show More Posts</a>
    <div class="posts__spinner">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
</div>
  </div>
</div>