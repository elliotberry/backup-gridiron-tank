	<?php
			/**
			 * Sticky Post: setup post data and render
			 * */
				$sticky = get_option( 'sticky_posts' );
				if ($sticky):
				$sticky_post_ID = $sticky[0];
				$post = get_post($sticky_post_ID);
				if (isset($post)):
			?>			
			<div class="sticky sticky-posts">
				<div class="container">
					<?php  
						
						global $post;
						setup_postdata($post);
						get_template_part( 'template-parts/post-list-item');
						wp_reset_postdata();				
					?>
				</div>
			</div>
			<?php endif; ?>						
			<?php endif; ?>