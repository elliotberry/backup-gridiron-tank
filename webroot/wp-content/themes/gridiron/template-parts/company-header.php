<div class="container">
  <div class="row">

    <div class="col-md">
    <div class="company--title heading page-title"><?php the_title(); ?></div>

    <div class="block--content-block__subtitle company--subtitle_info">
      
        <?php 
        if (get_field('current_or_past') == 'past') { ?>
          <div class="company--partner-since"><strong>Past Investment</strong></div>
       <?php }
       else {
        field_div("company--partner-since", "partnered_date", "<strong>Partner Since: </strong><span>{% field %}</span>"); ?>
        
        <?php 
       }
        field_div("company--location", "location", "<strong>HQ: </strong><span>{% field %}</span>"); ?>
        <div class="company--industry">
          <?php 
          $industry_terms = get_the_terms($post->ID, 'industry');
          $industry_string = '';
          foreach ($industry_terms as $term) {
            $industry_string .= $term->name.' ';
          }
          ?>
          <strong>Industry: </strong><span><?php echo $industry_string; ?></span>
        </div>
    </div>

    <?php 
    field_div("company--description block--content-block__body", "description");
    ?>
    </div>
    <div class="company--image col-md">
        <?php gridiron_post_thumbnail(); ?>
    </div>
  </div>
</div>