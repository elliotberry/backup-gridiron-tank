<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gridiron
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('gridiorn-news-post-single'); ?>>
	<div class="entry-content">
	<div class="container">
		<div class="post__title row">
			<h1 class="col-md-6">
				<?php the_title(); ?>
			</h1>
		</div>
		<div class="post__date"><?php echo get_the_date( 'F Y' ); ?></div>
		<div class="post__category">
			<?php gridiron_render_post_categories(get_the_ID(), true); ?>
		</div>
		<div class="post__content">
			<?php the_content(); ?>
		</div>
	</div><!-- .entry-content -->

	<?php gridiron_render_blocks() ?>

</article><!-- #post-<?php the_ID(); ?> -->
