<?php 
$showBreadcrumbs = false;
$breadcrumbs = array();

// Function for making a breadcrumb object based on a page/posts ID.
function makeBreadcrumb($id){
  $crumb = new stdClass();
  $crumb->title = get_the_title($id);
  $crumb->url =  get_the_permalink($id);
  return $crumb;
}

if (is_page()) {
  // If its a page, check and see if the page has a parent, if so, display that as the breadcrumb. Only works for one level. 
  $this_ID = get_the_ID();
  $parent = wp_get_post_parent_id($this_ID);
  if ($parent && $parent != 0) {
    $crumb = makeBreadcrumb($parent);
    array_push($breadcrumbs, $crumb);
    $showBreadcrumbs = true;
  }




} else if (is_single()) {
  // If its a single post type, setup breadcrumbs based on pre-defined page hierarchy 
  // TODO.. setup ACF to define the breadcrumbs for posts types.

  $type = get_post_type();
  $parents = false;

  if ($type == 'post') {
    $parents = array(get_option( 'page_for_posts' ));
  }
  if ($type == 'company') {
    $parents = array(143, 140);
  }
  if ($type == 'case_studies') {
    $parents = array(143);
  }
  if ($type == 'team_bio') {
    $parents = array(135, 147);
  }

  if ($parents) {
    foreach ($parents as $parent) {
      $crumb = makeBreadcrumb($parent);
      array_push($breadcrumbs, $crumb);
    }
    $showBreadcrumbs = true;
  }
}


?>
<?php if($showBreadcrumbs): ?>
  <section class="gridiron-breadcrumbs container">
    <?php foreach ($breadcrumbs as $crumb):?>
      <span class="breadcrumb"><a href="<?php echo $crumb->url ?>"><?php echo $crumb->title ?></a></span>
    <?php endforeach; ?>
  </section>
<?php endif; ?>