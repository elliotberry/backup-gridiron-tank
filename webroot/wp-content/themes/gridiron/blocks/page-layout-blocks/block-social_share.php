
 <?php
 $p = get_the_permalink();
 $t = urlencode(get_the_title());
 $linkz = array();
 $linkz['facebook'] = '<a class="social-link-item facebook" href="https://www.facebook.com/sharer/sharer.php?u=' . $p . '" alt="Share on Facebook"></a>';
 $linkz['twitter'] = '<a class="social-link-item twitter" href="https://twitter.com/home?status=' . $p . '" alt="Share on Twitter"></a>';
 $linkz['linkedin'] = '<a class="social-link-item linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=' . $p . '&title=' . $t . '&summary=&source=' . $p . '" alt="Share on LinkedIn"></a>';
?>
   <section class="block--social-share">
<div data-aos="fade-up" class="container">
            <!-- Field: Title. Type: text 
            -->
            <div class="block--social-share__title">
                <?php if (get_sub_field('title')) { 
                    the_sub_field('title');
                }?>
            </div>
            <div class="social-links">
                <?php if (get_sub_field('links')): 
                $lns = get_sub_field('links');
                foreach ($lns as $link) {
                   
                    echo $linkz[$link];
                 
                }
            endif;
            ?>
            </div>
            </div>
            </div>
 </section>

 
 
    <!-- /.block-contact-form  -->