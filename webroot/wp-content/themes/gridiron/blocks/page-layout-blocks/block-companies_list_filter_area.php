<?php

$args = array(
    'post_type' => 'company',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
);

$postlist = get_posts($args);

function phpDateFunc($obj) {
  if (get_field('partnered_date', $obj->ID)) {
    $dated = get_field('partnered_date', $obj->ID, false);
  }
  else {
    $dated = "19011101";
  }
  $obj->raw_date = $dated;
  $ret = new DateTime($dated);
  $obj->date_var = $ret;
  return $ret;
}

function sortByDate($x, $y) {
  $dx = phpDateFunc($x);
  $dy = phpDateFunc($y);
  if ($dx < $dy) {
    $retu = 1; 
  } elseif ($dx > $dy)  {
    $retu = -1; 
  } else {
    $retu = 0; 
  }
  return $retu;
} ?>

<!-- <?php print_r($postlist); ?> -->

<?php
usort($postlist, 'sortByDate');
?>

<!-- <?php print_r($postlist); ?> -->
    <div class="companies__filter">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="label">Show: </div>
            <div class="select-wrap">
              <select class="option-status">
                <option value="all" selected>All</option>
                <option value="current">Current</option>
                <option value="past">Past</option>
              </select>
            </div>

          </div>
          <div class="col-md-4">
            <div class="label">Industry: </div>
            <div class="categories">


                <?php
                  $industry_terms_list = get_terms(array(
                    'taxonomy' => 'industry',
                    'hide_empty' => true,
                  ) );
                ?>
              <form action="#" method="post" id="filter">
                <div class="select-wrap">
                  <select class="option-industry">
                  <option class="company-industry-all" value="all" selected>All</option>

                  <?php foreach ($industry_terms_list as $term): ?>
                  <option class="company-industry-<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>">
                    <?php echo $term->name; ?>
                  </option>
                  <?php endforeach; ?>

                  </select>
                </div>
                <input type="hidden" name="action" value="company_filter">
              </form>

              <span class="expand"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="companies-list">
      <div class="container">
        <div class="row">
        <?php if ($postlist): $i = 0;?>

        <?php foreach ($postlist as $block): setup_postdata($block);
        $post = $block; 
        get_template_part( 'template-parts/company-list-item'); 

    endforeach;
    wp_reset_postdata();
    ?></div>
      </div>
      <?php endif;?>