
<section class="block--news-content">
  <div data-aos="fade" class="container">
    <div>
      <?php if (get_sub_field('content')) {
      the_sub_field('content');
      }?>
    </div>
  </div>
</section>