
<?php 
$posts = array();

    $args = array( 
        'post_type'  => 'team_bio',
        'posts_per_page' => 3,
        'orderby'        => 'rand'
    );
    $the_query = new WP_Query( $args ); 
    

// The Loop
if ( $the_query->have_posts() ) {
	
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
        ?>
         <div class="col related-person">
         <?php field_div("person--image block--related-posts__image", "image", '<img src="{% field %}" />'); ?>
        <div class="related-posts-desc">
         <?php field_div("person--name block--related-posts__name", "name"); ?>
         <?php field_div("person--title block--related-posts__title", "title"); ?> 
        </div>
    
        </div>
       <?php 
	}
	/* Restore original Post Data */
    wp_reset_postdata();
}
?>