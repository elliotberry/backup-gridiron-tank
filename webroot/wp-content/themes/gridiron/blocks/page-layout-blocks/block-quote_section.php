<!-- Quote Section block Template!!!! 
CSS Name: .block-quote-section

-->
   <div class="block--quote-section">
   <div data-aos="zoom" class="container">

            <!-- Field: Quote Headline. Type: text 
            -->
            <?php if (get_sub_field('quote_headline')) { ?>
            <div class="block--quote-section__quote-headline">
                <?php  the_sub_field('quote_headline'); ?>
                <hr />
            </div>
            <?php } ?>            

            <!-- Field: Quote. Type: text 
            -->
            <div class="block--quote-section__quote">
                <?php if (get_sub_field('quote')) { 
                    the_sub_field('quote');
                }?>

                
            </div>
            <!-- Field: Quote Link. Type: link 
            -->
            <div class="block--quote-section__quote-link">
                <?php if (get_sub_field('quote_link')) { 
                    the_sub_field('quote_link');
                }?>

                
            </div>
            <!-- Field: Quote Attribution. Type: text 
            -->
            <div class="block--quote-section__quote-attribution">
                <?php if (get_sub_field('quote_attribution')) {
                    echo '<span class="quote-attribution">';
                    the_sub_field('quote_attribution');
                }?><?php if (get_sub_field('quote_attribution') && get_sub_field('attribution_subheading')) { 
                    echo ',</span>';
                } else {
                    echo '</span>';
                }?>
                <?php if (get_sub_field('attribution_subheading')) { 
                    the_sub_field('attribution_subheading'); 
                }?>
            </div>
            <div class="block--quote-section__attribution-subheading">
                
            </div>
            </div>
 </div>

    <!-- /.block-quote-section  -->