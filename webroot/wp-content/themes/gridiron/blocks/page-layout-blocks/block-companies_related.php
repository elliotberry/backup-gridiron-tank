<?php 
$posts = get_sub_field('posts');

if( $posts ):
    foreach( $posts as $post): 
    setup_postdata($post); 
        get_template_part( 'template-parts/company-list-item'); 
    endforeach;  
    wp_reset_postdata(); 
endif;

?>


