<!-- CTA Block block Template!!!! 
CSS Name: .block-cta-block
-->
<div class="block--cta-block mammma">
<?php 
$title = get_sub_field('c_cta_title');
$link = get_sub_field('g_cta_link');
?>
<div data-aos="fade-down" data-aos-duration="300" class="block--cta-block__title">
    <?php if($title): 
        echo $title;
    elseif ($link): ?>
        <a href="<?php echo $link['url'] ?>" class="block--cta-block__title-link"><?php echo $link['title'] ?></a> 
   <?php endif; ?>
</div>

<?php if ($link && $title) { 
?>
<a href="<?php echo $link['url'] ?>" data-aos="flip-left" class="cta-button block--cta-block__link"><?php echo $link['title'] ?></a> 
<?php } ?>
    

</div>

<!-- /.block-cta-block  -->