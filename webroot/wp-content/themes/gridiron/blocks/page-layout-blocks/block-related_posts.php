<!-- Related Posts block Template!!!!
CSS Name: .block-related-posts

-->
<?php $type = get_post_type(); ?>
   <div class="block--related-posts">
<div data-aos="fade" class="container<?php if($type == 'team_bio'): echo ' team-meet-more'; endif; ?>">
<h2 class="block--related-posts__title"><?php the_sub_field('title'); ?></h2>
    <div class="row">
<?php


if ($type == "company") {
    include 'block-companies_related.php';
}
elseif ($type == "team_bio") {
    $posts = get_sub_field('posts');

    if( $posts ):
        foreach( $posts as $post): 
        setup_postdata($post); 
        $post_id = get_the_ID();
		$img_url = get_the_post_thumbnail_url($post_id);
            ?>
                <a href="<?php the_permalink(); ?>" class="col-md team-meet-more__member">
                    <div class="team-meet-more__member-image">
                        <img src="<?php echo $img_url; ?>">
                    </div>
                    <h3><?php the_field('name'); ?></h3>
                    <h4><?php the_field('title'); ?></h4>
                </a> <!-- team-meet-more__member -->
            <?php
        endforeach;  
        wp_reset_postdata(); 
    endif;
    }
?>

              </div>
            </div>
        </div>
 </div>

    <!-- /.block-related-posts  -->