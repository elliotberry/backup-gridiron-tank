<!-- Text Over Image Block
CSS Name: .block--text-over-image
-->
<div class="block--text-over-image">
    <?php if (get_sub_field('background_image')) {$bgi = get_sub_field('background_image');?>
        <div class="block--text-over-image__background-image" style="background-image:url('<?php echo $bgi['url']; ?>');">
        </div>
    <?php }?>
   <div data-aos="fade" class="container">
        <div class="block--text-over-image__text">
            <?php  the_sub_field('text'); ?>
        </div>
    </div>
 </div>

    <!-- /.block--text-over-image  -->