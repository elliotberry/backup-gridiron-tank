<section class="block-case_studies_list">
    <div class="container">
<?php 
foreach (get_sub_field('case_studies_featured') as $key => $value) {
    $post = get_post($value);
    global $post;
    setup_postdata($post);
    $company = get_field('company_referenced');
    $company_id = $company[0]->ID;
    $link = get_the_permalink();
    $logo = get_field('logo', $company_id);
    $name = get_the_title();
    $desc = get_field('list_item_description');
    $image = get_the_post_thumbnail_url();
    ?>

        <div class="row case-study">
            <div data-aos="fade-up" class="col-md-6 info">
                <div class="info-in">
                    <img class="company-story-logo" src="<?php echo $logo['url']; ?>" />
                    <div class="company-story-desc">
                        <?php echo $desc; ?>
                    </div>
                    <a class="cta-button" href="<?php echo $link; ?>">Read <?php echo $name; ?>'s Story</a>
                </div>
            </div>
            <div data-aos="fade-up" data-aos-delay="200" class="col-md-6 company-story-image">
                <div class="image-in">
                    <img class="company-story-logo" src="<?php echo $image; ?>" />
                </div>
            </div>
        </div>    

    
<?php
wp_reset_postdata();

}
?>
    </div>
</section>