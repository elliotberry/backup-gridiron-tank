
   <div class="block--content-columns">
       <div class="container">
            <!-- Field: Section Title. Type: text 
            -->
            <div class="block--content-rows__section-title">
                <?php if (get_sub_field('section_title')) { 
                    the_sub_field('section_title');
                }?>

                
            </div>

<div class="row">
<?php $delay = 0; ?>
<?php while ( have_rows('column') ) : the_row(); ?>
    <div class="col content--col" 
        data-aos="fade-up"
        data-aos-easing="ease-out"
        data-aos-duration="500"
        data-aos-delay="<?php echo $delay; ?>"
    >
        <div class="block--content-columns__title"><?php the_sub_field('title'); ?></div>
        <div class="block--content-columns__text"><?php the_sub_field('text'); ?></div>
    </div>
    <?php $delay = $delay + 200; ?>

<?php endwhile; ?>
    </div>

</div>      
 </div>
