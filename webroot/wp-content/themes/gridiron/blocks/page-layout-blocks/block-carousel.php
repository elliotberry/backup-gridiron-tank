<section data-aos="zoom" class="block--carousel">
    <div class="container">


    <?php 
    $type = get_sub_field('carousel_type');
    
    if ($type == 'company') {
        echo '<h2>' . get_field('companies_carousel_title', 'options') . '</h2>';
    }
    else {
        if (get_sub_field('carousel_title')) {?>
            <h2 class="carousel-title"><?php the_sub_field('carousel_title'); ?></h2>
            <?php 
        } 
    }
    $i = 0;


if ($type !== "images") {
    $query = new WP_Query(array('post_type' => $type,'posts_per_page' => -1, 'orderby' => 'menu_order' ));
    // The Loop
    if ($query->have_posts()) {
        echo '<div class="slider slider-'.$type. '">';
        while ($query->have_posts()) {
            $query->the_post();
            $post_id = get_the_ID();
            $img_url = get_the_post_thumbnail_url($post_id);

            if ($type == 'company') {
                $logo = get_field( 'logo' );
                $img_url = $logo['url'];
            } 

            ?>
            
            <div class="gridiron-slide">
            <a href="<?php the_permalink(); ?>" > <img class="block--carousel__image" src="<?php echo $img_url;  ?>" /></a>
        </div>
   


            <?php
        
        } echo "</div>";
      
        /* Restore original Post Data */
        wp_reset_postdata();
    } 
} else {

    if (have_rows('carousel_item')): ?>

    <div class="slider gridiron-image-slider">
    <?php while (have_rows('carousel_item')): the_row();?>
            <div class="gridiron-slide">
                <a href="#" class="gridiron-slide-in" data-featherlight="<?php the_sub_field('image');?>"><img class="block--carousel__image" title="<?php the_sub_field('caption');?>" src="<?php the_sub_field('image');?>" /></a>
            </div>
            <?php
$i++;
endwhile;
echo "</div>";
?>
    </div>
<?php endif; } ?>

<?php if (get_sub_field('carousel_cta')) {?>
                        <?php $cta = get_sub_field('carousel_cta') ?>
                        <a href="<?php echo $cta['url'] ?>" class="block--content-block__cta"><?php echo $cta['title'] ?></a> 
                    <?php }?>


</section>
