<!-- Contact Form block Template!!!! 
CSS Name: .block-contact-form

-->
<section class="block--news-company-information">
  <div data-aos="fade-up" class="container">
    <div class="row">
      <?php field_div("news-information-about-company", "about_company"); ?>
    </div>
    <div class="row">
      <?php 
      if (get_sub_field('show_gridiron_company_info')) {
      ?>
      <div class="news-info-gridiron-information col-md-7">
        <?php the_field('gridiron_company_information', 'option'); ?>
        </div>
      <?php
      } 

      if (get_sub_field('show_gridiron_contact_info')) {
      ?>
      <div class="news-info-gridiron-contact col-md-5">
        <?php the_field('gridiron_contact', 'option'); ?>
      </div>
      <?php
      } 
      ?>
    </div>
  </div>
 </section>

 
 
    <!-- /.block-contact-form  -->