<div class="block__about-gridiron-client">
    <div class="container">
        <?php if (get_sub_field('about_the_client')): ?>
            <div class="row">
                <div class="col about-gridiron-col">
                    <?php the_sub_field('about_the_client'); ?>
                </div>
            </div>
        <?php endif; ?>        
        <?php if (get_sub_field('add_gridiron_info') == 1): ?>


        <div class="row">
            <div class="col about-gridiron-col">
                <?php the_field('gridiron_company_information', 'option'); ?>
            </div>
            <div class="col about-gridiron-col">
                <?php the_field('gridiron_contact', 'option'); ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>