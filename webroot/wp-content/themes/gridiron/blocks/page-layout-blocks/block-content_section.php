<!-- Content Block block Template!!!!
CSS Name: .block-content-block

-->
<?php

$align = get_sub_field('image_alignment');
if ($align == "left") {
    $img_class="order-1";
    $text_class="order-2";
}
else {
    $img_class="order-2";
    $text_class="order-1";
}

?>
   <section <?php if(get_sub_field('content_section_id')): echo 'id="' . get_sub_field('content_section_id') . '"'; endif; ?> class="block--content-block <?php if (get_sub_field('section_style')) {
    the_sub_field('section_style');
}
if (get_sub_field('remove_images_on_mobile') == 1) {
    echo " img-removed-on-mobile";
}
?>">
<div class="container">
    <div class="row h-100 justify-content-between">
<?php if (get_sub_field('background_image')) {$bgi = get_sub_field('background_image');?>
    <div class="block--content-block__background-image" style="background-image:url('<?php echo $bgi; ?>');">
    </div>
<?php }?>

<div class="col-md-7 content-column <?php echo $text_class; ?>">
            <!-- Field: Title. Type: text
            -->
            <?php if (get_sub_field('title')) { ?>
                <h2 data-aos="fade-up" class="block--content-block__title "><?php the_sub_field('title'); ?></h2>
            <?php }?> 
                

     
            <!-- Field: Body. Type: wysiwyg
            -->
            <?php if (get_sub_field('body')) { ?>
                <div data-aos="fade-up" class="block--content-block__body "><?php the_sub_field('body'); ?></div>
            <?php }?>             

                    <?php if (get_sub_field('cta')) {?>
                        <?php $cta = get_sub_field('cta') ?>
                        <a data-aos="fade-up" href="<?php echo $cta['url'] ?>" class="block--content-block__cta"><?php echo $cta['title'] ?></a> 
                    <?php }?>
                </div>

                <div class="col-md <?php echo $img_class; ?> img-block">
                    <?php 
                    if (get_sub_field('images') || get_sub_field('image')) {
                   
               
                    if (get_sub_field('images')) {
                        $images = get_sub_field('images');
                        $num = count($images);
                        $item_num = rand(0, ($num -1));
                        $this_image = $images[$item_num]['image'];
                    }
                    else {
                        $this_image = get_sub_field('image');
                    }
                       
                        $imgSrcArray = wp_get_attachment_image_src($this_image, 'large'); 
                        ?>
                            <div data-aos="fade" data-aos-delay="500" class="block--content-block__image">
                            <img src="<?php echo $imgSrcArray[0] ?>">
                        </div>
                        <?php
                
                 }
                    ?>
                </div>

</div>
            </div>
</section>

    <!-- /.block-content-block  -->