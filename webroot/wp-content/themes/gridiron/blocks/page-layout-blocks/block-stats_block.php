<div class="block--stats-block">
    <div class="container">
    <div class="stats-container">
    <?php $delay = 0; ?>
    <?php while ( have_rows('stat') ) : the_row(); ?>
        <div data-aos="zoom-in-right" data-duration="250" data-aos-delay="<?php echo $delay; ?>" class="row stat-row">
            <div class="block--stats-block__title col-sm-4"><?php the_sub_field('title'); ?></div>
            <div class="block--stats-block__text  col-sm-8"><?php the_sub_field('content'); ?></div>
        </div>
    <?php $delay = $delay + 100; ?>
    <?php endwhile; ?>
    </div>
    </div>
</div>