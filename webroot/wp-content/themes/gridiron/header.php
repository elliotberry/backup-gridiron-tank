<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gridiron
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); 
	get_template_part( 'template-parts/favicons'); ?>
</head>

<body <?php body_class(); ?>>
	<header data-aos="fade" data-aos-duration="300" id="header" class="header">
		<div class="header-container">
		<div class="top-nav">
					<div class="container">
						<?php wp_nav_menu( array(
						'container'				=> false,
						'menu_class'     	=> 'nav-topnav-menu',
						'menu_id'        	=> 'topnav-navigation',
						'menu'           	=> 'Topnav Navigation'
						)); ?>
						<div class="search-bar">
							<form class="search-form" action="<?php echo home_url(); ?>" method="get">
							<input type="search" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="Search">
							<input type="submit" name="" value="">
							</form>
						</div>
					</div>
				</div>			
			<div class="logo-container container">
				<?php $logo = get_field('options_logo', 'option'); ?>

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="header__logo">
					<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" class="header__logo-image" />
				</a>
			</div>
			<div id="gridiron-main-nav" class="navwrap">
				<div class="navbar">
					<div class="container">
						<?php wp_nav_menu( array(
							'container'				=> false,
							'menu_class'     	=> 'nav-main-menu',
							'menu_id'        	=> 'main-navigation',
							'menu'           	=> 'Main Navigation'
						)); ?>
						<?php $cta = get_field('options_header_cta', 'option'); ?>
						<a id="header_cta_button" href="<?php echo $cta['url']; ?>" alt="<?php echo $cta['title']; ?>" class="cta header__cta cta--blue"><?php echo $cta['title']; ?></a>
					</div>
				</div>
			</div>
			<div class="mobile-nav-toggle">
				<span></span>
				<span></span>
				<span></span>
			</div>


		</div>

	</header>
