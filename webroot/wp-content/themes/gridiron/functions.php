<?php
/**
 * gridiron functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gridiron
 */

if (!function_exists('gridiron_setup')):
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function gridiron_setup()
{
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on gridiron, use a find and replace
         * to change 'gridiron' to the name of your theme in all the template files.
         */
        load_theme_textdomain('gridiron', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'gridiron'),
            'footer_menu' => esc_html__('Footer', 'gridiron'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('gridiron_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;

add_action('after_setup_theme', 'gridiron_setup');

/* Saves ACF config to JSON automatically. */
function my_acf_json_save_point($path)
{

    // update path
    $path = get_template_directory() . '/acf-json';

    // return
    return $path;

}
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */

function gridiron_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('gridiron_content_width', 640);
}

add_action('after_setup_theme', 'gridiron_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function gridiron_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'gridiron'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'gridiron'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'gridiron_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function gridiron_scripts()
{
    wp_enqueue_style('gridiron-style', get_template_directory_uri() . '/assets/css/style.min.css');
    wp_enqueue_script('gridiron-javascript-main', get_template_directory_uri() . '/assets/js/main.min.js');
    
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }


    	// need team member count for AJAX load more
  //	$team_posts = wp_count_posts('team_bio')->publish;
  	// how many team members to load initially?
  //	$init_team_count = get_field('number_of_bios_to_load', 'option') ? get_field('number_of_bios_to_load', 'option') : 10;
  	//$max_team_pages = round($team_posts/$init_team_count);

	// need success story count for AJAX load more
 // 	$story_post_count = wp_count_posts('success_story')->publish;
  	// how many sucess stories to load initially?
  //	$init_story_count = get_field('number_of_stories_to_load', 'option') ? get_field('number_of_stories_to_load', 'option') : 10;
  //	$max_story_pages = round($story_post_count/$init_story_count);

  	// since we are filtering and doing Load More, we need to know how many posts of a given category are already loaded
  	//$category_offset = '';

	wp_localize_script( 'gridiron-javascript-main', 'gridiron', array(
		'ajaxurl' => "/wp-admin/admin-ajax.php",
		'cookie_domain' => "VESON_COOKIE_DOMAIN",
    	//'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
    	//'max_page' => $max_team_pages,
    	//'max_story_page' => $max_story_pages,
    	//'story_post_count' => $story_post_count
	));
    
}

add_action('wp_enqueue_scripts', 'gridiron_scripts');

#Instead of having to repeat the if statement for the field each time, we can invoke this.
function field_div($div, $name, $inside = "") {

    if (get_field($name) || get_sub_field($name)) {
        if (get_field($name)) {
            $field = get_field($name);
        }
        else {
            $field = get_sub_field($name);
        }
        if ($inside !== "") {
            $div_inside = str_replace("{% field %}", $field, $inside);
        }
        else {
            $div_inside = $field;
        }
        echo '<div class="' . $div . '">' . $div_inside . '</div>';
    }
    else {
        return false;
    }
}



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Post Types
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * ACF Options Page
 */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

/** Custom Theme Functions */

function gridiron_image($filename)
{
    return get_template_directory_uri() . '/assets/images/' . $filename;
}

// Loop through blocks and render the according php block include file.

function gridiron_render_blocks()
{
    if (!function_exists('have_rows')) {
        return;
    }

    if (!have_rows('content_blocks')) {
        return;
    }

    while (have_rows('content_blocks')): the_row();
        $block = get_row_layout();
        
        get_template_part('blocks/page-layout-blocks/block', $block);
    endwhile;
}


add_filter( 'body_class', 'sp_body_class' );
function sp_body_class( $classes ) {
	if (have_rows('content_blocks')) {
        while (have_rows('content_blocks')): the_row();
        $block = get_row_layout();
            $classes[] = "has-block--" . $block;
        endwhile;

    }
	$classes[] = 'custom-class';
	return $classes;
	
}

function returnStickyCat() {
    $minus_cat = "";
    $sticky = get_option( 'sticky_posts' );
    if ($sticky):
    $sticky_post_ID = $sticky[0];
    $post = get_post($sticky_post_ID);
    if (isset($post)):
        $minus_cat = get_the_category()[0]->slug;
    endif;
endif;
return $minus_cat;
}


function getPostCatInfo() {

$cat_minus = returnStickyCat();

$cats = get_categories();

foreach ($cats as $cat) {
    if ($cat->slug == $cat_minus) {
        $cat_post_number = $cat->category_count - 1;
    }
    else {
        $cat_post_number = $cat->category_count;
    }
    echo " " . createSlug($cat->name) . ": ";
    echo "{";
    echo '"id": ' . $cat->term_id . ','; 
    echo '"name": "' . $cat->name . '",';
    echo '"count": ' . $cat_post_number;
    echo "},";
}
if ($cat_minus != "") {
    $full_count = wp_count_posts('post')->publish - 1;
}
else {
    $full_count = wp_count_posts('post')->publish;
}
echo "all: {id:0, count: " . $full_count . "}"; 
}

function createSlug($str, $delimiter = '-'){

    $unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
        'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
    $str = strtr( $str, $unwanted_array );

    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    return $slug;
}

function gridiron_render_post_categories($postID, $returnLink = false ){
    $post_categories = wp_get_post_categories($postID);
    $cats = array();
    foreach($post_categories as $c){
        $cat = get_category( $c );
        $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
        if ($cat->term_id != 1) {
            if ($returnLink) {
                echo '<a class="post-cat post-cat-link">' . $cat->name . '</a>';
            } else {
                echo '<span class="post-cat">' . $cat->name . '</span>';
            }
        }
    }
}



/**
 * Load Posts
 */
require get_template_directory() . '/inc/posts-load-posts.php';


function hide_editor()
{
    remove_post_type_support('page', 'editor');
}

add_action('admin_head', 'hide_editor');
add_filter('show_admin_bar', '__return_false');


/**
 * Disable Comments
 */

 function remove_admin_menus () { 
    global $menu; 
    $removed = array(
        __('Comments')
        ); 
    end ($menu); 
    while (prev($menu)){ 
        $value = explode(
                ' ',
                $menu[key($menu)][0]); 
        if(in_array($value[0] != NULL?$value[0]:"" , $removed)){
            unset($menu[key($menu)]);
        }
    } 
} 
add_action('admin_menu', 'remove_admin_menus');

// Excerpts to pages 

add_post_type_support( 'page', 'excerpt' );