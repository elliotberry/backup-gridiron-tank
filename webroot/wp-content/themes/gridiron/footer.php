<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gridiron
 */

 // $options_instagram = get_field('options_instagram', 'options');
 // $options_twitter   = get_field('options_twitter', 'options');
 // $options_facebook  = get_field('options_facebook', 'options');
 // $options_youtube   = get_field('options_youtube', 'options');

 	$copyright = get_field('options_copyright', 'option');
 ?>

<div class="footer-newsletter block--newsletter-block">
  <div data-aos="zoom" class="container">
  <div id="newsletterButton" class="blue-button">Sign up for our newsletter</div>
  <div id="form-container">
    <?php echo do_shortcode('[contact-form-7 id="445" title="Newsletter Sign-up"]'); ?>
  </div>
  </div>            
</div>

<footer id="site-footer" class="footer">

	<div class="container">
    <div class="row">
      <div class="col-md-5 footer-logo-copyright">
        <?php $logo = get_field('options_footer_logo', 'option'); ?>

        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="footer__logo">
          <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" class="footer__logo-image" />
        </a>
        
        <div class="footer-copyright">
          <?php the_field('options_copyright', 'option'); ?>
        </div>
      </div>
      <div class="col-md">
        <?php wp_nav_menu( array(
          'container'      => false,
          'menu_class'     => 'footer__nav',
          'menu_id'        => 'footer',
          'menu'           => 'Footer Menu'
        )); ?>
      </div>
      <div class="footer-cta col-md-4">
        <?php $footerPDF = get_field('options_investment_criteria_pdf', 'option'); ?>
        <a href="<?php echo $footerPDF['url']; ?>" class="block--content-block__cta cta footer-cta-button" download>Download the Gridiron Overview</a>
        </div>
      <div class="col-md">
      <?php include(locate_template('blocks/block-modules/social-media-icons.php')); ?>
      </div>

  
    </div>


	</div> <!--/.container -->
</footer>

<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/components/aos.js"></script>
	<script>
    setTimeout(function(){ 
      AOS.init({
      once: true,
      duration: 600,
      easing: 'ease-out-sine',
      disable: 'phone'
    });      
    }, 200);

	</script>
</body>
</html>
<!-- ******* -->

