<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gridiron
 */

get_header();
?>

	<div id="primary" class="content-area page-news-list">
		<main id="main" class="site-main">
		<?php
		if (have_posts()):
			if ( is_home() && ! is_front_page() ) :
				?>
				<header class="container">
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;
			?>
			<?php get_template_part( 'template-parts/sticky-posts'); ?>
			<?php
			/**
			 * Setup and render flex content blocks defined in posts page
			 * */
			?>	

			<div class="posts-page-blocks">
				<?php 
					$posts_page_ID = get_option( 'page_for_posts' );
					$post = get_post($posts_page_ID);
					global $post;
					setup_postdata($post);
					gridiron_render_blocks();
					wp_reset_postdata();
				?>
			</div>
			<?php
				$init_post_count = get_field('number_of_news_articles_to_load', 'option') ? get_field('number_of_news_articles_to_load', 'option') : 10;
				$args = array(
					'post_type' => 'post',
					'posts_per_page' => $init_post_count,
					'post_status' => 'publish',
					'orderby'=>'date',
					'order'=>'DESC',
					'post__not_in' => get_option( 'sticky_posts' )
				);

				$postlist = get_posts($args);
			?>

			
				<?php
				get_template_part( 'template-parts/post-list-filter');
				 if (isset($postlist)) { ?>


			<div class="posts__list-container container"> 
					<?php foreach($postlist as $block): setup_postdata($block);
					$post = $block;
					?>

				<?php get_template_part( 'template-parts/post-list-item'); ?>
			
				<?php
				   endforeach;
					wp_reset_postdata();
					?>
				</div>
				 <?php }
				get_template_part( 'template-parts/load-more-posts'); 
				?>
			</div>

	    <?php
		
		else:
			get_template_part( 'template-parts/content', 'none' );
		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
<script>

<?php get_template_part( 'template-parts/load-posts.js'); ?>

</script>
<?php
get_footer();
