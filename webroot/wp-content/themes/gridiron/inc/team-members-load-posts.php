<?php

/* --------------------------------------------
 *
 * AJAX LOAD TEAM MEMBERS
 *
 * -------------------------------------------- */
add_action( 'wp_ajax_loadposts', 'ajax_loadposts_handler' );
add_action( 'wp_ajax_nopriv_loadposts', 'ajax_loadposts_handler' );

function ajax_loadposts_handler() {

  $init_team_count = get_field('number_of_bios_to_load', 'option') ? get_field('number_of_bios_to_load', 'option') : 10;

  $args = array(
    'posts_per_page' => $init_team_count,
    'post_status' => 'publish',
    'orderby'=>'date',
    'order'=>'DESC',
    'post_type' =>'team_bio'
  );

  $max_pages = '';
  $content = '';

  if( $_POST['page'] ) $args['paged'] = $_POST['page'] + 1;

  $team_posts = new WP_Query( $args );
  $i = 1;

  if( ! $team_posts->have_posts() ) {
      get_template_part( 'template-parts/content', 'none' );
  }
  else {

      while ( $team_posts->have_posts() ) {
          $team_posts->the_post();
          $image = get_the_post_thumbnail_url(get_the_ID(), 'full');
          $name = get_the_title(get_the_ID());
          $position = get_field('position', get_the_ID());
          $city = get_field('city', get_the_ID());
          ?>

          <div class="col-6 col-md-2 our-team--custom-grid">
            <div class="our-team__item">
              <?php if($image): ?>
                <a href="<?php echo get_post_permalink($block); ?>">
                  <div class="our-team__image" style="background-image: url('<?php echo $image; ?>');"></div>
                </a>
              <?php endif; ?>
              <div class="our-team__name">
                <a href="<?php echo get_post_permalink($block); ?>"><?php echo $name; ?></a>
              </div>
              <div class="our-team__position">
                <?php echo $position; ?>
              </div>
              <div class="our-team__city">
                <?php echo $city; ?>
              </div>
            </div>
          </div>
        <?php $i++;
      }
  }
  die();
}
