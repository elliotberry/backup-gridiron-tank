<?php

/* --------------------------------------------
 *
 * Register post types
 *
 * -------------------------------------------- */

    //
    // @link http://codex.wordpress.org/Function_Reference/register_post_type
    //



if( !function_exists('sk_post_types') ) :
/**
 * register content types
 */
function sk_post_types(){

    register_post_type('company', array(
        'labels'        => array(
            'name'               => __( 'Companies', 'gridiron' ),
            'singular_name'      => __( 'Company', 'gridiron' ),
            'add_new_item'       => __( 'Add new Company', 'gridiron' ),
            'edit_item'          => __( 'Edit Company', 'gridiron' ),
            'new_item'           => __( 'New Company', 'gridiron' ),
            'view_item'          => __( 'View Company', 'gridiron' ),
            'search_items'       => __( 'Search Companies', 'gridiron' ),
            'not_found'          => __( 'No Companies found', 'gridiron' ),
            'not_found_in_trash' => __( 'No Companies found in Trash', 'gridiron' ),

        ),
        'supports'      => array('title', 'thumbnail', 'excerpt'),
        'hierarchical'  => true,
        'public'        => true,
        'taxonomies'    => array( 'industry')
    ));


    register_post_type('team_bio', array(
        'labels'        => array(
            'name'               => __( 'Team Bios', 'gridiron' ),
            'singular_name'      => __( 'Team Bio', 'gridiron' ),
            'add_new_item'       => __( 'Add new Team Bio', 'gridiron' ),
            'edit_item'          => __( 'Edit Team Bio', 'gridiron' ),
            'new_item'           => __( 'New Team Bio', 'gridiron' ),
            'view_item'          => __( 'View Team Bio', 'gridiron' ),
            'search_items'       => __( 'Search Team Bios', 'gridiron' ),
            'not_found'          => __( 'No Team Bios found', 'gridiron' ),
            'not_found_in_trash' => __( 'No Team Bios found in Trash', 'gridiron' ),

        ),
        'supports'      => array('title', 'thumbnail', 'excerpt'),
        'hierarchical'  => true,
        'public'        => true,
        'taxonomies'    => array('team_category')

    ));
 
    register_post_type('case_studies', array(
        'labels'        => array(
            'name'               => __( 'Case Studies', 'gridiron' ),
            'singular_name'      => __( 'Case Study', 'gridiron' ),
            'add_new_item'       => __( 'Add new Case Study', 'gridiron' ),
            'edit_item'          => __( 'Edit Case Study', 'gridiron' ),
            'new_item'           => __( 'New Case Study', 'gridiron' ),
            'view_item'          => __( 'View Case Study', 'gridiron' ),
            'search_items'       => __( 'Search Case Studies', 'gridiron' ),
            'not_found'          => __( 'No Case Studies found', 'gridiron' ),
            'not_found_in_trash' => __( 'No Case Studies found in Trash', 'gridiron' ),

        ),
        'supports'      => array('title', 'thumbnail', 'excerpt'),
        'hierarchical'  => true,
        'public'        => true,
        'taxonomies'    => array( 'industry')

    ));

}

add_action('init', 'sk_post_types');
endif; // sk_post_types


/* 
CUSTOM TAXONOMIES
*/
function create_gridiron_taxonomies() {
	$labels = array(
		'name'              => _x( 'Team Category', 'taxonomy general name' ),
		'singular_name'     => _x( 'Team Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Team Categories' ),
		'all_items'         => __( 'All Team Categories' ),
		'parent_item'       => __( 'Parent Team Category' ),
		'parent_item_colon' => __( 'Parent Team Category:' ),
		'edit_item'         => __( 'Edit Team Category' ),
		'update_item'       => __( 'Update Team Category' ),
		'add_new_item'      => __( 'Add New Team Category' ),
		'new_item_name'     => __( 'New Team Category Name' ),
		'menu_name'         => __( 'Team Categories' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'team-category' ),
	);

register_taxonomy( 'team_category', array('team_bio'), $args );

$labels = array(
    'name'              => _x( 'Industry', 'taxonomy general name' ),
    'singular_name'     => _x( 'Industry', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Industries' ),
    'all_items'         => __( 'All Industries' ),
    'parent_item'       => __( 'Parent Industry' ),
    'parent_item_colon' => __( 'Parent Industry:' ),
    'edit_item'         => __( 'Edit Industry' ),
    'update_item'       => __( 'Update Industry' ),
    'add_new_item'      => __( 'Add New Industry' ),
    'new_item_name'     => __( 'New Industry Name' ),
    'menu_name'         => __( 'Industries' ),
);

$args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'industry' ),
);

register_taxonomy( 'industry', array('company', 'case_studies'), $args );

}

add_action( 'init', 'create_gridiron_taxonomies', 0 );