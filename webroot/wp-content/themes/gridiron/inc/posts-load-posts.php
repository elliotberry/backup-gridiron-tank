<?php

/* --------------------------------------------
 *
 * AJAX LOAD POSTS
 *
 * -------------------------------------------- */
add_action( 'wp_ajax_loadposts', 'ajax_loadposts_handler' );
add_action( 'wp_ajax_nopriv_loadposts', 'ajax_loadposts_handler' );

function ajax_loadposts_handler() {
  
  $init_post_count = get_field('number_of_news_articles_to_load', 'option') ? get_field('number_of_news_articles_to_load', 'option') : 10;
  if(isset($_POST['offset'])) {
    $offset = $_POST['offset'];
  }
  else {
    $offset = $init_post_count;
  }
  $args = array(
    'posts_per_page' => $init_post_count,
    'post_status' => 'publish',
    'orderby'=>'date',
    'order'=>'DESC',
    'post_type' =>'post',
     'offset' => $offset,
     'post__not_in' => get_option( 'sticky_posts' )
  );

  if( isset($_POST['cat']) ) {
    $args['cat'] = $_POST['cat'];
  }

  $max_pages = '';
  $content = '';

  if( $_POST['page'] ) $args['paged'] = $_POST['page'] + 1;

  $post_posts = new WP_Query( $args );
  $i = 1;

  if( ! $post_posts->have_posts() ) {
      get_template_part( 'template-parts/content', 'none' );
  }
  else {

      while ( $post_posts->have_posts() ) {
          $post_posts->the_post();
          
          get_template_part( 'template-parts/post-list-item');
         $i++;
      }
  } 
  die();
}
