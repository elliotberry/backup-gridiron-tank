<?php

/* --------------------------------------------
 *
 * AJAX LOAD COMPANIES
 *
 * -------------------------------------------- */

add_action( 'wp_ajax_loadcompanies', 'ajax_loadcompanies_handler' );
add_action( 'wp_ajax_nopriv_loadcompanies', 'ajax_loadcompanies_handler' );

function ajax_loadcompanies_handler() {

  $init_story_count = get_field('number_of_companies_to_load', 'option') ? get_field('number_of_companies_to_load', 'option') : 10;

  $args = array(
    'posts_per_page' => $init_story_count,
    //'offset' => $init_story_count,
    'post_status' => 'publish',
    'orderby'=> 'date',
    'order'=> 'DESC',
    'post_type' => 'company',
    //'cat' => 10
  );

  $max_pages = '';
  $content = '';

  // see if something besides the "All" filter is being used
  $filter_category = false;
  $filter_category_name = get_cat_name( (int)$_POST['filter_category'] );
  if( $filter_category_name && strtolower($filter_category_name) !== 'all' )
    $filter_category = (int)$_POST['filter_category'];

  if( $filter_category !== false ) { 
    $args['cat'] = $filter_category;
    $args['posts_per_page'] = -1; 
  }

  if( strtolower($filter_category_name) === 'all' ) {
    $args['posts_per_page'] = $init_story_count;
  }

  if( count( $_POST['current_posts'] ) ) { $args['post__not_in'] = $_POST['current_posts']; }

  //error_log('posts!!!: '.$_POST['current_posts'][0] );

  //error_log( print_r( $$args['post__not_in'] ) );

  if( $_POST['page'] ) $args['paged'] = $_POST['page'] + 1;

  $success_posts = new WP_Query( $args );

  //error_log('count: '.$success_posts->post_count);
  error_log('sql: '.$success_posts->request);

  if( !$success_posts->have_posts() ) { 
      get_template_part( 'template-parts/content', 'none' );
  }
  else {
    while ( $success_posts->have_posts() ) { 
          $success_posts->the_post(); 
          $category = get_the_category(get_the_ID());
          ?>

          <div class="companies-collection__item col-md-6 <?php if ( ! empty( $category ) ) echo 'success-story-cat-'.$category[0]->term_id; ?>" data-post-id="<?php echo get_the_ID(); ?>">
            <div class="row">
              <div class="col-md-5"> 
                <a href="<?php echo get_post_permalink($block)?>">
                  <div class="companies-collection__image" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium');?>')"></div>
                </a>
              </div>


              <div class="companies-collection__content col-md-6">
                <div class="companies-collection__content--category">
                  <?php if ( ! empty( $category ) ) echo $category[0]->cat_name; ?>
                </div>

                <div class="companies-collection__content--title">
                  <a href="<?php echo get_post_permalink(get_the_ID())?>">
                  <?php echo get_the_title(get_the_ID()); ?>
                  </a>
                </div>

                <div class="companies-collection__content--subtitle">
                  <?php echo get_field('subtitle', get_the_ID()); ?>
                </div>
                <div class="companies-collection__content--cta">
                  <a href="<?php echo get_post_permalink(get_the_ID()); ?>" class="cta cta--min-gold"></a>
                </div>
              </div>
            </div>
          </div>
      <?php }
  }
  die();
}
