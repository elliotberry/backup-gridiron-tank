<?php
/**
 * Template Name: Contact Page
 *
 * @package gridiron
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
	<div class="container">
	<div class="row">
	<div class="col-5">
	<h1 class="page-title">
		<?php the_title(); ?>
	</h1>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	
	<div class="contact-text">
		<?php the_field('contact_page_text'); ?>
	</div>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

		<div class="block--content-block__image mobileImage">
			<img src="<?php echo $image[0]; ?>">
		</div>
	<?php endif; ?>
	<?php echo do_shortcode('[contact-form-7 id="369" title="Gridiron Contact Form"]'); ?>
	</div>
	<div class="col-7">
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

	<div class="block--content-block__image desktopImage">
		<img src="<?php echo $image[0]; ?>">
	</div>
  </div>
<?php endif; ?>

	</div>
	
	</div>
	<div class="contact-page-address">
		<div class="container">
			<div class="row">
				<div class="col-5">
					<h3>Address</h3>
					<?php the_field('contact_page_address'); ?>
				</div>
				<div class="col-5">
					<h3>Phone</h3>
					<?php the_field('contact_page_phone'); ?>
				</div>
			</div>
		</div>
	</div>
	</div> 
		<?php
		while ( have_posts() ) :
			the_post();
		
			

		endwhile; // End of the loop.
		?>

	
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
