import '../css/style.scss';
import Bowser from './components/bowser/bowser.js';
import './components/jquery.bxslider.min.js';
import './components/featherlight.js';
import Macy from './components/macy.js';
import './components/companies-filters.js';
import './components/nav.js';

const browser = Bowser.getParser(window.navigator.userAgent);
const browserInfo = browser.getBrowser();


//Runs functions by-page in WP so we don't get annoying console errors.
function onPage(bodyClass, cb) {
	if ($('body').hasClass(bodyClass)) {
		cb();
	}
}

$(function () {
	onPage('has-block--carousel', function() {
		var slideNumber = 3;
		if ($(window).width() < 930) {
			slideNumber = 1;
		}
		$('.slider').bxSlider({
			captions: true,
			speed: 300,
			slideMargin: 40,
			pager: false,
			slideWidth: 900,
			minSlides: slideNumber,
			maxSlides: slideNumber,
			moveSlides: 1,
			responsive: true,
			shrinkItems: true,
			onSliderLoad: function() {
				AOS.refresh();
			},
			onSliderResize: function() {
				AOS.refresh();
			}
		});
	});
	
	onPage('page-template-page-team-php', function() {
		/*
		Team Profile Filters
		Hacked this together by cloning the grid of profiles into a hidden container, then filtering 
		by emptying the visible container then populating it with the filtered profiles and recalculating Macy
		*/

		// Clone the content from the grid container into a hidden container
		const bios = $('.team-grid-container').html();
		$('.team-grid-container-hidden').html(bios);

		// $('.team-grid-container-hidden').show();
		// $('.team-grid-container-hidden .team-grid-item').css({
		// 	width: '10%',
		// 	float: 'left'
		// });

		
		
		// Init macy
		window.masonry = new Macy({
			container: '.team-grid-container',
			trueOrder: false,
			waitForImages: false,
			useOwnImageLoader: false,
			mobileFirst: false,
			columns: 3,
			margin: 16,
			breakAt: {
				1300: 3,
				940: 2,
				600: 1
			}
		});

		// Filter function: populates the visiblie conainer with only filtered profiles
		// Accepts a classname to filter as the argument
		function filterTeam(filter){
			const container = $('.team-grid-container');
			container.removeClass("is--loaded").empty();

			// Give it 300ms to allow the css "is--loaded" animation to run.
			setTimeout(function(){ 
				$('.team-grid-container-hidden .team-grid-item').each(function(){
					const el = $(this);
					if (el.hasClass(filter)) {
						const clone = el.clone();
						container.append(clone);					
					}
				});		
			}, 300);
			// Give Masonry a little time to re-calculate
			setTimeout(function(){ 
				window.masonry.recalculate(true, false);
				container.addClass('is--loaded');
				AOS.refresh();
			}, 350);

		}

		// Select dropdown action, filter based on option val
		$("select.option-team-category").on("change", function() {
			const filter = $(this).val();
			filterTeam(filter)
		});
		
		// Initial reveal once images are loaded.
		window.masonry.on(window.masonry.constants.EVENT_IMAGE_COMPLETE, function (ctx) {
			$('.team-grid-container').addClass("is--loaded");
		});
	});
	onPage('custom-class', function () {
		const newsletterButton = document.getElementById('newsletterButton');
		const formContainer = document.getElementById('form-container');
		newsletterButton.addEventListener('click', function() {
			newsletterButton.classList.add('transform');
			setTimeout(() => {
				newsletterButton.classList.add('shrink');
				newsletterButton.textContent = 'Submit';
			}, 600);
			setTimeout(() => {
				newsletterButton.classList.add('shrink');
			}, 800);
			setTimeout(() => {
				newsletterButton.classList.add('disappear');
			}, 1000);
			setTimeout(() => {
				formContainer.classList.add('show');
			}, 1000);
		});
	});
});


$(document).ready(function(){
	// $('.mobile-navbar').click(function(){
	// 	$('body').addClass('mobile-nav-open');
	// });
	// $('.navwrap .mobile-close').click(function(e){
	// 	e.preventDefault();
	// 	$('#mobile-navbar--check').prop('checked', false);
	// 	$('body').removeClass('mobile-nav-open');
	// });
});

