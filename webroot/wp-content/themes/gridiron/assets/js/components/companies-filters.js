$(document).ready(function(){
  var companyArgs = {
      industry: "",
      status: ""
  };
    // Default SELECT values to ALL
    $('.option-industry').val('all');
    $('.option-status').val('all');

    // Helper to see if a substring exists
    function stringIncludes(string, term) {
      if (string.indexOf(term) != -1) {
        return true;
      }
      return false;
    }

    function setCompaniesDisplay() {
      $(".company-list-item").removeClass("is--active");
      $('.company-list-item').each(function(){
        var el = $(this);
        var industry_string = el.data('industries');
        var status_string = el.data('status');
        var industry_match = stringIncludes(industry_string, companyArgs.industry);
        var status_match = stringIncludes(status_string, companyArgs.status);
        if (industry_match && status_match) {
          el.addClass('is--active');
        }
        AOS.refresh();
      });
    }
    
    $(".companies__filter select").on("change", function() {
      companyArgs.industry = $(".option-industry").val();
      companyArgs.status = $(".option-status").val();
      setCompaniesDisplay();
    });  
});