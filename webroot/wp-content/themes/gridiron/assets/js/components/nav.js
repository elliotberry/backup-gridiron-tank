(function(gridironNAV, $) {
  function initNavNav() {
    $('#main-navigation').append($('#header_cta_button'));
    $('.mobile-nav-toggle').click(function(){
      $(this).toggleClass('open');
      $('body').toggleClass('mobile-nav-open');
    });

  }

  //
  // init
  //
  gridironNAV.init = function() {
    initNavNav();
  };

  $(document).ready(function(){ gridironNAV.init(); });
})((window.gridironNAV = window.gridironNAV || {}), jQuery);


