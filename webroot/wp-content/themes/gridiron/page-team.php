<?php
/**
 * Template Name: Team Bio List
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gridiron
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
while (have_posts()):
    get_template_part('template-parts/breadcrumbs');
    the_post();
    gridiron_render_blocks();

endwhile; // End of the loop.
?>


<div class="our-team">
  <div class="container">
    <div class="row team-filters-wrap">
    <?php
$team_terms_list = get_terms(array(
    'taxonomy' => 'team_category',
    'hide_empty' => true,
));
?>
    <div class="col-md-4">
  <div class="select-wrap">
    <select class="option-team-category">
      <option class="team-category-all" value="team-grid-item">All</option>

      <?php foreach ($team_terms_list as $term): ?>
      <option class="team-category-<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>">
      <?php echo $term->name; ?>
      </option>
      <?php endforeach;?>

    </select>
    </div>
    </div>
  </div>

  <div class="team-grid-container">
<?php
$args = array(
    'post_type' => 'team_bio',
    'posts_per_page' => -1,
);
$the_query = new WP_Query($args);
// The Loop
if ($the_query->have_posts()) {
    $bool = true;
    while ($the_query->have_posts()) {
        $the_query->the_post();

        $the_id = get_the_ID();
        $cls = $bool ? 'even' : 'odd';
        $img = get_field('image');
        $sizecls = "";
        if ($img) {
            $theimg = $img['url'];
            if ($img['width'] > $img['height']) {
                $sizecls = "img-wide";
            } else {
                $sizecls = "img-high";
            }
        } else {
            $theimg = get_stylesheet_directory_uri() . "/assets/img/blue-box.svg";
        }
        $team_cat_terms = get_the_terms($the_id, 'team_category');
        $team_cat_string = '';
        if ($team_cat_terms) {
            foreach ($team_cat_terms as $term) {
                $team_cat_string .= $term->slug . ' ';
            }
        }
        ?>
        <div class="team-grid-item <?php echo $cls; echo " " . $sizecls; echo " " . $team_cat_string; ?>">
          <a href="<?php the_permalink();?>">
           <img src="<?php echo $theimg; ?>" />
          <div class="team-border-overlay">
            <div class="team-border-overlay__text">
              <h4><?php the_field('name');?></h4>
              <div><?php the_field('title');?></div>
            </div>
          </div>
          </a>
        </div>

<?php
$bool = !$bool;
}
wp_reset_postdata();
}
?>



  </div>
  <div class="team-grid-container-hidden"></div>



		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
