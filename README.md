

Gridiron Website
=====================

Wordpress custom theme based on [underscores](https://github.com/Automattic/_s/). Uses node/npm/webpack for asset (js/scss) management.


Installation
---------------

1. Run `npm install` to install front-end dependencies.

2. If using vagrant for development, run `vagrant up` from the root directory to provision a development server. [Scotchbox](https://box.scotch.io/) is used by default so refer to the website for server default settings (mysql passwords, etc.)

3. SSH into the development server using `vagrant ssh` to setup the wordpress database. `mysql -u root -p` using the password "root" and create a new database `CREATE DATABASE gridiron;`

4. Import a mysql database dump into your new database

5. Copy `webroot/wp-content/wp-sample-config.php` to `wp-config.php` and add the appropriate database credentials.



Development
---------------

Webpack will watch and compile both Sass and ES6 Javascript into CSS and ES5 respectively.

Run `npm run dev` to watch files and compile upon change. This should be used when developing.

Run `npm run build` to build the files for production.

Javascript assets can be found in `webroot/wp-content/themes/gridiron/assets/js`. The webpack entry file is `main.js` and is compiled to `main.min.js`

Sass/CSS assets can be found in `webroot/wp-content/themes/gridiron/assets/css`. The webpack entry file is `style.scss` and is compiled to `style.min.css`